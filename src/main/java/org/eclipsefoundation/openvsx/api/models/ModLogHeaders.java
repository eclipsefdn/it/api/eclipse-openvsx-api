/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.api.models;

import jakarta.ws.rs.HeaderParam;

/**
 * Model used to capture X-ModLog-* header params.
 */
public class ModLogHeaders {
    @HeaderParam("X-ModLog-Key-1")
    private String key1;
    @HeaderParam("X-ModLog-Key-2")
    private String key2;
    @HeaderParam("X-ModLog-table")
    private String table;
    @HeaderParam("X-ModLog-Person")
    private String person;
    @HeaderParam("X-ModLog-Action")
    private String action;
    @HeaderParam("X-ModLog-Time")
    private String time;

    public ModLogHeaders() {
        this.key1 = "api.eclipse.org";
        this.table = "openvsx/publisher_agreement";
    }

    public ModLogHeaders(String key2, String person, String action) {
        this("api.eclipse.org", key2, "openvsx/publisher_agreement", person, action, null);
    }

    public ModLogHeaders(String key1, String key2, String table, String person, String action, String time) {
        this.key1 = key1;
        this.key2 = key2;
        this.table = table;
        this.person = person;
        this.action = action;
        this.time = time;
    }

    public String getKey1() {
        return key1;
    }

    public void setKey1(String key1) {
        this.key1 = key1;
    }

    public String getKey2() {
        return key2;
    }

    public void setKey2(String key2) {
        this.key2 = key2;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
