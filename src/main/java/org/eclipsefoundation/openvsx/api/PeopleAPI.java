/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.api;

import java.util.List;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentData;
import org.eclipsefoundation.openvsx.api.models.ModLogHeaders;

import io.quarkus.oidc.client.filter.OidcClientFilter;
import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

/**
 * Fdndb-api binding for People resources.
 */
@Path("people")
@OidcClientFilter
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "fdndb-api")
@ApplicationScoped
public interface PeopleAPI {

    /**
     * Create a person record into foundationDB. Only status code is used to validate success of the operation.
     * 
     * @param src The person to create
     * @return A Response containing the persisted person.
     */
    @PUT
    @RolesAllowed("fdb_write_people")
    Response persistPersonEntity(PeopleData src);

    /**
     * Fetches a single person from foundationDb using the given personId.
     * 
     * @param personId The given personID
     * @return A PeopleData entity if found
     */
    @GET
    @Path("{personID}")
    @RolesAllowed("fdb_read_people")
    PeopleData getPerson(@PathParam("personID") String personId);

    /**
     * Fetches a list of all PeopleDocument entities tied to a given user filtered by documentID.
     * 
     * @param personId The given ef username
     * @param documentId The desired document id
     * @return A List of PeopleDocumentData entities if they exist
     */
    @GET
    @Path("{personID}/documents")
    @RolesAllowed("fdb_read_people_documents")
    List<PeopleDocumentData> getPeopleDocument(@PathParam("personID") String personId, @QueryParam("documentID") String documentId,
            @QueryParam("include_bytes") boolean includeBytes);

    /**
     * Persists a PeopleDocumentData entity in foundationDb. Can be used to create new documents or update existing ones. Passes the
     * required ModLog headers to allow automatic ModLogEvent tracking.
     * 
     * @param modLog The required ModLog headers
     * @param personId The person signing the document
     * @param src The signed document(s).
     * @return A List of PeopleDocumentData entites if the operation was successful
     */
    @PUT
    @Path("{personID}/documents")
    @RolesAllowed("fdb_write_people_documents")
    List<PeopleDocumentData> persistPeopleDocument(@BeanParam ModLogHeaders modLog, @PathParam("personID") String personId,
            PeopleDocumentData src);
}
