/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.config;

import java.util.List;

import io.smallrye.config.ConfigMapping;
import io.smallrye.config.WithDefault;

/**
 * Config loader for document and admin user validation.
 */
@ConfigMapping(prefix = "eclipse.openvsx.publisher-agreement")
public interface PublisherAgreementConfig {

    String docId();

    @WithDefault("1")
    Double docVersion();

    List<String> adminUsers();
}
