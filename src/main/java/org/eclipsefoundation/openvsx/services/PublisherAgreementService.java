/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.services;

import java.util.Optional;

import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentData;
import org.eclipsefoundation.openvsx.models.PublisherAgreementData;

/**
 * Defines the service for fetching, creating and revoking openvsx publisher agreements.
 */
public interface PublisherAgreementService {

    /**
     * Given the username, retrieves the most recent valid openvsx publisher agreement for the given user.
     * 
     * @param username The desired user's EF username.
     * @return An Optional containing a PublisherAgreementData entity if it exists.
     */
    public Optional<PublisherAgreementData> getPublisherAgreementByUsername(String username);

    /**
     * Creates a publisher agreement for the given user using the request body containing the user GH handle and document version. Inserts a
     * modLog event on fail or pass. Returns an empty Optional if it encounters an error.
     * 
     * @param user The current logged in user
     * @return An Optional containing the created PublisherAgreementData entity if it exists
     */
    public Optional<PublisherAgreementData> createPublisherAgreement(EfUser user);

    /**
     * Expires a Publisher agreement and updates it in the foundationdb. Inserts a modLog event on fail or pass. Returns an empty Optional
     * if it encounters an error.
     * 
     * @param document The current signed publisher agreement
     * @param currentUser The user bound to the token
     * @return An Optional containing the updated PeopleDocumentData if it exists
     */
    public Optional<PeopleDocumentData> revokePublisherAgreement(PeopleDocumentData document, String currentUser);
}
