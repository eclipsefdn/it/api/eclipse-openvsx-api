/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.services.impl;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentDataBuilder;
import org.eclipsefoundation.openvsx.api.models.ModLogHeaders;
import org.eclipsefoundation.openvsx.config.PublisherAgreementConfig;
import org.eclipsefoundation.openvsx.models.DocumentBody;
import org.eclipsefoundation.openvsx.models.PublisherAgreementData;
import org.eclipsefoundation.openvsx.namespace.OpenvsxModLogActions;
import org.eclipsefoundation.openvsx.services.FoundationOperationService;
import org.eclipsefoundation.openvsx.services.PublisherAgreementService;
import org.eclipsefoundation.utils.helper.DateTimeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class DefaultPublisherAgreementService implements PublisherAgreementService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultPublisherAgreementService.class);

    @Inject
    PublisherAgreementConfig config;

    @Inject
    FoundationOperationService foundationService;

    @Inject
    ObjectMapper objectMapper;

    @Override
    public Optional<PublisherAgreementData> getPublisherAgreementByUsername(String username) {
        Optional<PeopleDocumentData> mostRecent = foundationService.fetchMostRecentDocument(username);
        return mostRecent.isEmpty() || isdocumentExpired(mostRecent.get()) ? Optional.empty()
                : Optional.of(buildPublisherAgreement(mostRecent.get(), true));
    }

    @Override
    public Optional<PublisherAgreementData> createPublisherAgreement(EfUser user) {
        try {
            LOGGER.debug("Creating publisher agreement for user: {}", user.name());

            ModLogHeaders headers = new ModLogHeaders(user.name(), user.name(), OpenvsxModLogActions.AGREEMENT_SIGNED);

            LOGGER.debug("Encoding publisher agreement document for user: {}", user.name());

            // Create document from user data + request info. Then encode it
            String jsonDoc = objectMapper
                    .writeValueAsString(new DocumentBody(String.valueOf(config.docVersion()), user.githubHandle(), user.name(),
                            user.firstName(), user.lastName(), user.mail()));

            Optional<PeopleDocumentData> creationResult = foundationService
                    .persistDocumentWithModLog(headers, createDocumentSigningRequest(jsonDoc, user.name()));

            // Convert to PublisherAgreementData if persistence successful
            return creationResult.isEmpty() ? Optional.empty() : Optional.of(buildPublisherAgreement(creationResult.get(), false));

        } catch (Exception e) {
            LOGGER.error("Error while creating publisher agreement", e);
            foundationService.insertErrorSysModLog("_openvsx_publisher_agreement_create", user.name());
            return Optional.empty();
        }
    }

    @Override
    public Optional<PeopleDocumentData> revokePublisherAgreement(PeopleDocumentData document, String currentUser) {
        try {
            LOGGER.debug("{} is revoking publisher agreement for user: {}", currentUser, document.personID());

            // Update the doc with an expiry date
            PeopleDocumentData updated = PeopleDocumentDataBuilder
                    .builder()
                    .personID(document.personID())
                    .documentID(document.documentID())
                    .version(document.version())
                    .effectiveDate(document.effectiveDate())
                    .receivedDate(document.receivedDate())
                    .scannedDocumentFileName(document.scannedDocumentFileName())
                    .scannedDocumentMime(document.scannedDocumentMime())
                    .scannedDocumentBLOB(document.scannedDocumentBLOB())
                    .scannedDocumentBytes(document.scannedDocumentBytes())
                    .comments(document.comments())
                    .expirationDate(new Date())
                    .build();

            ModLogHeaders headers = new ModLogHeaders(document.personID(), currentUser, OpenvsxModLogActions.AGREEMENT_REVOKED);

            return foundationService.persistDocumentWithModLog(headers, updated);

        } catch (Exception e) {
            LOGGER.error("Error while revoking publisher agreement", e);
            foundationService.insertErrorSysModLog("_openvsx_publisher_agreement_delete", document.personID());
            return Optional.empty();
        }
    }

    /**
     * Checks if the document is expired. True if expired, false if not.
     * 
     * @param document The current document.
     * @return True if expired/invalid, false otherwise
     */
    private boolean isdocumentExpired(PeopleDocumentData document) {
        boolean isExpired = document.expirationDate() != null && document.expirationDate().before(Date.from(Instant.now()));
        if (isExpired) {
            LOGGER.warn("Most recent document is expired");
        }
        return isExpired;
    }

    /**
     * Creates an OpenVSX publisher agreement document signing request using the given user. Builds and encodes the current Publisher
     * Agreement from the necessary fields.
     * 
     * @param jsonDoc The encoded Document as a Json String
     * @param username The user signing the agreement
     * @return The constructed PeopleDocumentData object.
     */
    private PeopleDocumentData createDocumentSigningRequest(String jsonDoc, String username) {
        LOGGER.debug("Creating document signing request for user: {}", username);

        List<Byte> docAsBytes = Arrays.asList(ArrayUtils.toObject(jsonDoc.getBytes()));

        // Set date to UTC timezone when creating
        Date now = Date.from(DateTimeHelper.now().toInstant());

        return PeopleDocumentDataBuilder
                .builder()
                .personID(username)
                .documentID(config.docId())
                .version(config.docVersion())
                .effectiveDate(now)
                .receivedDate(now)
                .scannedDocumentFileName("openvsx-publisher-agreement.json")
                .scannedDocumentMime("application/json")
                .scannedDocumentBLOB(docAsBytes)
                .scannedDocumentBytes(jsonDoc.length())
                .comments("Generated by api.eclipse.org/openvsx/publisher_agreement")
                .build();
    }

    /**
     * Builds a PublisherAgreementData object using the given PeopleDocumentData entity. Has a flag to offset the received date by one day
     * to compensate for the modification made by fdndb-api.
     * 
     * @param document The given PeopleDocumentData entity
     * @param adjustTime A Flag to increment the received date by a day
     * @return A populated PublisherAgreementData object
     */
    private PublisherAgreementData buildPublisherAgreement(PeopleDocumentData document, boolean adjustTime) {

        // Conversion of List<Byte> to byte[] to allow conversion to string
        byte[] blob = document.scannedDocumentBLOB() == null ? new byte[0]
                : ArrayUtils.toPrimitive(document.scannedDocumentBLOB().toArray(new Byte[document.scannedDocumentBLOB().size()]));

        SimpleDateFormat effectiveDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat receivedDateFormat = new SimpleDateFormat("yyyy-MM-dd");

        String receivedDate = "";

        // The 'received_date' column is only a 'date' type and gets rewinded a day when
        // adjusted for UTC time by fdndb-api. Flag should onlybe set on fetch requests
        if (adjustTime) {
            ZonedDateTime adjustedTime = ZonedDateTime.parse(DateTimeHelper.toRFC3339(document.receivedDate())).plusDays(1);
            receivedDate = receivedDateFormat.format(Date.from(adjustedTime.toInstant()));
        } else {
            receivedDate = receivedDateFormat.format(document.receivedDate());
        }

        return new PublisherAgreementData(document.personID(), document.documentID(), Integer.toString((int) document.version()),
                effectiveDateFormat.format(document.effectiveDate()), receivedDate, new String(blob), document.scannedDocumentMime(),
                Integer.toString(blob.length), document.scannedDocumentFileName(), document.comments());
    }
}
