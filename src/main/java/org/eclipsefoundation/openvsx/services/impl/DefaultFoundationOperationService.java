/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.services.impl;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.caching.service.CachingService;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysModLogData;
import org.eclipsefoundation.foundationdb.client.runtime.model.system.SysModLogDataBuilder;
import org.eclipsefoundation.http.helper.IPParser;
import org.eclipsefoundation.openvsx.api.PeopleAPI;
import org.eclipsefoundation.openvsx.api.SysAPI;
import org.eclipsefoundation.openvsx.api.models.ModLogHeaders;
import org.eclipsefoundation.openvsx.config.PublisherAgreementConfig;
import org.eclipsefoundation.openvsx.namespace.OpenvsxModLogActions;
import org.eclipsefoundation.openvsx.services.FoundationOperationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.Response.Status;

@ApplicationScoped
public class DefaultFoundationOperationService implements FoundationOperationService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultFoundationOperationService.class);

    @Inject
    PublisherAgreementConfig config;

    @RestClient
    PeopleAPI peopleAPI;
    @RestClient
    SysAPI sysAPI;

    @Inject
    CachingService cache;
    @Inject
    IPParser ipParser;

    @Override
    public Optional<PeopleDocumentData> fetchMostRecentDocument(String username) {
        try {
            LOGGER.debug("Fetching most recent publisher agreement for user: {}", username);

            List<PeopleDocumentData> results = peopleAPI.getPeopleDocument(username, config.docId(), true);
            if (results == null || results.isEmpty()) {
                LOGGER.warn("Unable to find agreement for user with name: {}", username);
                return Optional.empty();
            }

            // Sort by date. Most recent first
            return results.stream().sorted((pDoc1, pDoc2) -> pDoc2.effectiveDate().compareTo(pDoc1.effectiveDate())).findFirst();

        } catch (Exception e) {
            LOGGER.error("Error while fetching publisher agreement for user: {}", username, e);
            return Optional.empty();
        }
    }

    @Override
    public Optional<PeopleDocumentData> persistDocumentWithModLog(ModLogHeaders headers, PeopleDocumentData toInsert) {
        LOGGER.debug("Persisting publisher agreement for user: {}", toInsert.personID());

        List<PeopleDocumentData> results = peopleAPI.persistPeopleDocument(headers, toInsert.personID(), toInsert);
        if (results == null || results.isEmpty()) {
            LOGGER.error("Unable to create agreement for user with name: {}", toInsert.personID());
            return Optional.empty();
        }

        return Optional.of(results.get(0));
    }

    @Override
    public Optional<SysModLogData> insertErrorSysModLog(String pk1, String personId) {
        try {
            // All error ModLogs use client IP and log the failed action taken
            SysModLogData src = SysModLogDataBuilder
                    .builder()
                    .logTable("openvsx/publisher_agreement")
                    .pk1(pk1)
                    .pk2(ipParser.getBestMatchingIP())
                    .logAction(OpenvsxModLogActions.SQL_ERROR)
                    .personId(personId)
                    .modDateTime(ZonedDateTime.now())
                    .build();

            LOGGER.debug("Inserting error ModLog: {}", src);

            List<SysModLogData> results = sysAPI.insertSysModLog(src);
            if (results == null || results.isEmpty()) {
                LOGGER.error("Error while inserting error ModLog: {}", src);
                return Optional.empty();
            }

            return Optional.of(results.get(0));
        } catch (Exception e) {
            LOGGER.error("Error persisting ModLog", e);
            return Optional.empty();
        }
    }

    @Override
    public boolean createDbUserIfNotFound(EfUser user) {
        try {
            // No need to create new user if they already exist in fdndb
            Optional<PeopleData> fetchResult = fetchFoundationUser(user.name());
            if (fetchResult.isPresent()) {
                return true;
            }

            LOGGER.debug("User {} not found in fdndb. Creating new entry", user.name());

            PeopleData person = PeopleDataBuilder
                    .builder()
                    .personID(user.name())
                    .fname(user.firstName())
                    .lname(user.lastName())
                    .type("XX")
                    .member(false)
                    .email(user.mail())
                    .unixAcctCreated(false)
                    .issuesPending(false)
                    .build();

            // Insert new person record
            return peopleAPI.persistPersonEntity(person).getStatus() == Status.OK.getStatusCode();

        } catch (Exception e) {
            LOGGER.error("Error inserting user: {}", user.name(), e);
            insertErrorSysModLog("createFoundationUser", user.name());
            return false;
        }
    }

    /**
     * Fetches a user given a specific personId and returns an Optional containing the result if it exists.
     * 
     * @param personId The desired person's ID
     * @return an Optional containing the desired PeopleData if it exists.
     */
    private Optional<PeopleData> fetchFoundationUser(String personId) {
        try {
            LOGGER.debug("Fetching user: {}", personId);
            return cache.get(personId, new MultivaluedHashMap<>(), PeopleData.class, () -> peopleAPI.getPerson(personId)).data();
        } catch (Exception e) {
            LOGGER.error("Error searching for user: {}", personId, e);
            insertErrorSysModLog("fetchFoundationUser", personId);
            return Optional.empty();
        }
    }
}
