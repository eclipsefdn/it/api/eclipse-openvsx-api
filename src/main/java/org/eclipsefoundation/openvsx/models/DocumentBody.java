/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.models;

import java.util.Objects;

/**
 * Model used to represent the publisher agreement document. Gets serialized to a String then inserted into the DB as a Blob.
 * Every field except mail is required to be non-null.
 */
public record DocumentBody(String version, String githubHandle, String username, String firstName, String lastName, String mail) {

    public DocumentBody {
        Objects.requireNonNull(version);
        Objects.requireNonNull(githubHandle);
        Objects.requireNonNull(username);
        Objects.requireNonNull(firstName);
        Objects.requireNonNull(lastName);
    }
}
