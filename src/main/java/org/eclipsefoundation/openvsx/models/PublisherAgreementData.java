/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.models;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model used to capture outbound PA data for a user. All fields are required to be non-null.
 */
public record PublisherAgreementData(
    @JsonProperty("PersonID") String personId, 
    @JsonProperty("DocumentID") String documentId,
    @JsonProperty("Version") String version, 
    @JsonProperty("EffectiveDate") String effectiveDate,
    @JsonProperty("ReceivedDate") String receivedDate, 
    @JsonProperty("ScannedDocumentBLOB") String scannedDocumentBlob,
    @JsonProperty("ScannedDocumentMime") String scannedDocumentMime, 
    @JsonProperty("ScannedDocumentBytes") String scannedDocumentBytes,
    @JsonProperty("ScannedDocumentFileName") String scannedDocumentFiledName, 
    @JsonProperty("Comments") String comments) {

    public PublisherAgreementData {
        Objects.requireNonNull(personId);
        Objects.requireNonNull(documentId);
        Objects.requireNonNull(version);
        Objects.requireNonNull(effectiveDate);
        Objects.requireNonNull(receivedDate);
        Objects.requireNonNull(scannedDocumentBlob);
        Objects.requireNonNull(scannedDocumentMime);
        Objects.requireNonNull(scannedDocumentBytes);
        Objects.requireNonNull(scannedDocumentFiledName);
        Objects.requireNonNull(comments);
    }
}
