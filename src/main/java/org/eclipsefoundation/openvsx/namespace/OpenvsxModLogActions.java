/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.namespace;

public final class OpenvsxModLogActions {

    public static final String AGREEMENT_SIGNED = "OPENVSX PUBLISHER AGREEMENT SIGNED";
    public static final String AGREEMENT_REVOKED = "OPENVSX PUBLISHER AGREEMENT REVOKED";
    public static final String SQL_ERROR = "sql_error";

    private OpenvsxModLogActions() {
    }
}
