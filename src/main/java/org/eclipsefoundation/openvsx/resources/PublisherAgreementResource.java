/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.resources;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.eclipsefoundation.efservices.api.models.EfUser;
import org.eclipsefoundation.efservices.models.AuthenticatedRequestWrapper;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentData;
import org.eclipsefoundation.http.model.WebError;
import org.eclipsefoundation.openvsx.config.PublisherAgreementConfig;
import org.eclipsefoundation.openvsx.models.AgreementSigningRequest;
import org.eclipsefoundation.openvsx.models.PublisherAgreementData;
import org.eclipsefoundation.openvsx.services.FoundationOperationService;
import org.eclipsefoundation.openvsx.services.PublisherAgreementService;
import org.eclipsefoundation.utils.exception.FinalForbiddenException;

import jakarta.inject.Inject;
import jakarta.ws.rs.BadRequestException;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.ServerErrorException;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

@Path("publisher_agreement")
public class PublisherAgreementResource {

    private static final String NOT_FOUND_MSG_FORMAT = "Unable to find agreement for user: %s";

    @Inject
    PublisherAgreementConfig config;

    @Inject
    AuthenticatedRequestWrapper userProfile;

    @Inject
    PublisherAgreementService agreementService;

    @Inject
    FoundationOperationService foundationService;

    @GET
    public Response getAgreement() {

        // Uses currently logged in user. Only an onwer can fetch their agreement
        String username = userProfile.getUserPublicProfileData().name();

        // Fetch agreement for user
        Optional<PublisherAgreementData> result = agreementService.getPublisherAgreementByUsername(username);
        if (result.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_MSG_FORMAT, username));
        }

        return Response.ok(result).build();
    }

    @POST
    public Response createAgreement(AgreementSigningRequest body) {

        // Uses currently logged in user. Only an owner can create their agreement
        EfUser user = userProfile.getUserPublicProfileData();

        // Check if body format is correct
        validateSigningRequest(body);

        // Conflict if already signed agreement that isn't expired
        if (agreementService.getPublisherAgreementByUsername(user.name()).isPresent()) {
            return new WebError(Status.CONFLICT,
                    "The request could not be completed due to a conflict with the current state of the resource.").asResponse();
        }

        // Create user in fdndb if they don't exist
        if (!foundationService.createDbUserIfNotFound(user)) {
            throw new ServerErrorException("Internal Server Error", Status.INTERNAL_SERVER_ERROR);
        }

        // Attempt to persist the agreement data. Returning the result
        Optional<PublisherAgreementData> result = agreementService.createPublisherAgreement(user);
        if (result.isEmpty()) {
            throw new BadRequestException("Unable to create Publisher Agreement with current request parameters");
        }
        return Response.ok(result.get()).build();
    }

    @GET
    @Path("{efUsername}")
    public Response getAgreementForUser(@PathParam("efUsername") String username) {

        // The owner or admin can retrieve a specific agreement
        checkIfAdminOrSelf(username);

        Optional<PublisherAgreementData> result = agreementService.getPublisherAgreementByUsername(username);
        if (result.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_MSG_FORMAT, username));
        }

        return Response.ok(result).build();
    }

    @DELETE
    @Path("{efUsername}")
    public Response revokeAgreementForUser(@PathParam("efUsername") String username) {

        // The owner or admin can retrieve a specific agreement
        checkIfAdminOrSelf(username);

        Optional<PeopleDocumentData> fetchResult = foundationService.fetchMostRecentDocument(username);
        if (fetchResult.isEmpty()) {
            throw new NotFoundException(String.format(NOT_FOUND_MSG_FORMAT, username));
        }

        String currentUser = userProfile.getUserPublicProfileData().name();

        Optional<PeopleDocumentData> updateResult = agreementService.revokePublisherAgreement(fetchResult.get(), currentUser);
        if (updateResult.isEmpty()) {
            throw new ServerErrorException("Internal Server Error", Status.INTERNAL_SERVER_ERROR);
        }

        return Response.noContent().build();
    }

    /**
     * Validates the incoming signing request by ensuring the fields are in valid states, and that the user in the URL is the same as the
     * one bound to the token.
     * 
     * @param request The incoming singing request
     */
    private void validateSigningRequest(AgreementSigningRequest request) {
        if (!StringUtils.isNumeric(request.version()) || Double.parseDouble(request.version()) != config.docVersion()) {
            throw new BadRequestException("The version of the agreement is missing or invalid");
        }

        if (StringUtils.isBlank(request.githubHandle())) {
            throw new BadRequestException("The github_handle is missing or invalid");
        }

        // Ensure GH handle from current user same as in request body.
        if (!StringUtils.equalsIgnoreCase(userProfile.getUserPublicProfileData().githubHandle(), request.githubHandle())) {
            throw new BadRequestException("The github_handle does not match our records.");
        }
    }

    /**
     * Checks whether the token user is in the listed allowed users or if the token user is the same as the user in the request URL. Throws
     * a FinalForbiddenException if the user can't access this resource
     * 
     * @param urlUsername The username in the request URL
     * @return True if current user can access endpoint
     */
    void checkIfAdminOrSelf(String urlUsername) {
        // Reject request if current user is not in URL and they aren't an admin
        EfUser user = userProfile.getUserPrivateProfileData();
        if (!urlUsername.equalsIgnoreCase(user.name())
                && config.adminUsers().stream().noneMatch(email -> email.equalsIgnoreCase(user.mail()))) {
            throw new FinalForbiddenException(String.format("Access denied to resources for: %s", urlUsername));
        }
    }
}