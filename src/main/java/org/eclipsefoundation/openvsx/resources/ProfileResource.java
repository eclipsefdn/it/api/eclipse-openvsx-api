/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.resources;

import java.util.Arrays;

import org.eclipsefoundation.efservices.models.AuthenticatedRequestWrapper;

import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;

@Path("profile")
public class ProfileResource {

    @Inject
    AuthenticatedRequestWrapper userProfile;

    @GET
    public Response getProfileInfo() {
        // Returns the public profile data tied to the current user
        return Response.ok(Arrays.asList(userProfile.getUserPublicProfileData())).build();
    }
}
