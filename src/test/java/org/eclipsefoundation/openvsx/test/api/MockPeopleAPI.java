/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.test.api;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDataBuilder;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentData;
import org.eclipsefoundation.foundationdb.client.runtime.model.people.PeopleDocumentDataBuilder;
import org.eclipsefoundation.openvsx.api.PeopleAPI;
import org.eclipsefoundation.openvsx.api.models.ModLogHeaders;

import io.quarkus.test.Mock;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.MultivaluedHashMap;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;

@Mock
@RestClient
@ApplicationScoped
public class MockPeopleAPI implements PeopleAPI {

    private MultivaluedMap<String, PeopleDocumentData> peopleDocs;
    private Map<String, PeopleData> people;

    public MockPeopleAPI() {
        this.peopleDocs = new MultivaluedHashMap<>();

        this.peopleDocs.add("fakeuser", buildPeopleDocument("fakeuser", "sampleId"));
        this.peopleDocs.add("firstlast", buildPeopleDocument("firstlast", "sampleId"));
        this.peopleDocs.add("name", buildPeopleDocument("name", "otherId"));
        this.peopleDocs.add("username", buildPeopleDocument("username", "sampleId"));

        this.people = new HashMap<>();
        this.people.put("fakeuser", buildPeople("fakeuser"));
        this.people.put("firstlast", buildPeople("firstlast"));
        this.people.put("name", buildPeople("name"));
        this.people.put("username", buildPeople("username"));
    }

    @Override
    public Response persistPersonEntity(PeopleData src) {
        return Response.ok(Arrays.asList(people.put(src.personID(), src))).build();
    }

    @Override
    public PeopleData getPerson(String personId) {
        return people.get(personId);
    }

    @Override
    public List<PeopleDocumentData> getPeopleDocument(String personId, String documentId, boolean includeBytes) {
        return peopleDocs.get(personId).isEmpty() ? Collections.emptyList()
                : peopleDocs
                        .get(personId)
                        .stream()
                        .filter(pd -> pd.documentID().equalsIgnoreCase(documentId))
                        .collect(Collectors.toList());
    }

    @Override
    public List<PeopleDocumentData> persistPeopleDocument(ModLogHeaders modLog, String personId, PeopleDocumentData src) {

        if (peopleDocs.get(personId) != null && !peopleDocs.get(personId).isEmpty()) {

            // Remove if exists. Then add it
            Optional<PeopleDocumentData> existingDoc = peopleDocs
                    .get(personId)
                    .stream()
                    .filter(d -> d.personID().equalsIgnoreCase(personId) && d.documentID().equalsIgnoreCase(src.documentID())
                            && d.effectiveDate().compareTo(src.effectiveDate()) == 0)
                    .findFirst();
            if (existingDoc.isPresent()) {
                peopleDocs.remove(personId, existingDoc.get());
            }
        }

        peopleDocs.add(personId, src);

        return Arrays.asList(src);
    }

    private PeopleData buildPeople(String personId) {
        return PeopleDataBuilder
                .builder()
                .personID(personId)
                .fname("test")
                .lname("entity")
                .type("XX")
                .member(false)
                .email("test@mail.com")
                .unixAcctCreated(false)
                .issuesPending(false)
                .build();
    }

    private PeopleDocumentData buildPeopleDocument(String personId, String docId) {
        return PeopleDocumentDataBuilder
                .builder()
                .personID(personId)
                .documentID(docId)
                .version(1)
                .effectiveDate(new Date())
                .receivedDate(new Date())
                .scannedDocumentFileName("name")
                .scannedDocumentMime("application/json")
                .scannedDocumentBLOB(Arrays.asList(ArrayUtils.toObject("test blob".getBytes())))
                .scannedDocumentBytes(100)
                .comments("Some comments")
                .build();
    }
}
