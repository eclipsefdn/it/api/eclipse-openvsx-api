/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.resources;

import java.util.Map;
import java.util.Optional;

import org.eclipsefoundation.openvsx.models.AgreementSigningRequest;
import org.eclipsefoundation.openvsx.test.helpers.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import jakarta.inject.Inject;

@QuarkusTest
class PublisherAgreementResourceTest {
    public static final String BASE_URL = "publisher_agreement";
    public static final String USER_URL = BASE_URL + "/{efusername}";

    public static final Optional<Map<String, Object>> userCreds = Optional.of(Map.of("Authorization", "Bearer token2"));

    public static final Optional<Map<String, Object>> userNoDocCreds = Optional.of(Map.of("Authorization", "Bearer token5"));

    public static final Optional<Map<String, Object>> invalidCreds = Optional.of(Map.of("Authorization", "Bearer token1"));

    public static final Optional<Map<String, Object>> docCreateCreds = Optional.of(Map.of("Authorization", "Bearer token7"));

    public static final EndpointTestCase GET_CURRENT_SUCCESS = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaceHelper.PUBLISHER_AGREEMENT_SCHEMA_PATH)
            .setHeaderParams(userCreds)
            .build();

    public static final EndpointTestCase GET_CURRENT_NOT_FOUND = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(404)
            .setHeaderParams(userNoDocCreds)
            .build();

    public static final EndpointTestCase BAD_CREDS = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, null)
            .setStatusCode(403)
            .setHeaderParams(invalidCreds)
            .build();

    public static final EndpointTestCase POST_CURRENT_CONFLICT = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, null)
            .setStatusCode(409)
            .setHeaderParams(userCreds)
            .build();

    public static final EndpointTestCase POST_CURRENT_INVALID_HANDLE = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(400)
            .setHeaderParams(userCreds)
            .build();

    public static final EndpointTestCase GET_USER_SUCCESS = TestCaseHelper
            .prepareTestCase(USER_URL, new String[] { "fakeuser" }, SchemaNamespaceHelper.PUBLISHER_AGREEMENT_SCHEMA_PATH)
            .setHeaderParams(userCreds)
            .build();

    public static final EndpointTestCase GET_USER_NOT_FOUND = TestCaseHelper
            .prepareTestCase(USER_URL, new String[] { "name" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(404)
            .setHeaderParams(userNoDocCreds)
            .build();

    public static final EndpointTestCase FOR_USER_BAD_CREDS = TestCaseHelper
            .prepareTestCase(USER_URL, new String[] { "fakeuser" }, SchemaNamespaceHelper.PUBLISHER_AGREEMENT_SCHEMA_PATH)
            .setStatusCode(403)
            .setHeaderParams(invalidCreds)
            .build();

    public static final EndpointTestCase REVOKE_SUCCESS = TestCaseHelper
            .prepareTestCase(USER_URL, new String[] { "fakeuser" }, SchemaNamespaceHelper.PUBLISHER_AGREEMENT_SCHEMA_PATH)
            .setHeaderParams(userCreds)
            .setStatusCode(204)
            .build();

    public static final EndpointTestCase REVOKE_NO_DOC = TestCaseHelper
            .prepareTestCase(USER_URL, new String[] { "name" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(404)
            .setHeaderParams(userNoDocCreds)
            .build();

    public static final EndpointTestCase REVOKE_INVALID_USER = TestCaseHelper
            .prepareTestCase(USER_URL, new String[] { "other" }, SchemaNamespaceHelper.ERROR_SCHEMA_PATH)
            .setStatusCode(403)
            .setResponseContentType(ContentType.JSON)
            .setHeaderParams(userCreds)
            .build();

    @Inject
    ObjectMapper mapper;

    /*
     * GET CURRENT USER
     */
    @Test
    void testGet_currentUser_success() {
        EndpointTestBuilder.from(GET_CURRENT_SUCCESS).run();
    }

    @Test
    void testGet_currentUser_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_CURRENT_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGet_currentUser_success_validateSchema() {
        EndpointTestBuilder.from(GET_CURRENT_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGet_currentUser_failure_notFound() {
        EndpointTestBuilder.from(GET_CURRENT_NOT_FOUND).run();
    }

    @Test
    void testGet_currentUser_failure_notFound_validateResponseFormat() {
        EndpointTestBuilder.from(GET_CURRENT_NOT_FOUND).andCheckFormat().run();
    }

    @Test
    void testGet_currentUser_failure_notFound_validateSchema() {
        EndpointTestBuilder.from(GET_CURRENT_NOT_FOUND).andCheckSchema().run();
    }

    @Test
    void testGet_currentUser_failure_badCreds() {
        EndpointTestBuilder.from(BAD_CREDS).run();
    }

    /*
     * POST CURRENT USER
     */
    @Test
    void testPost_currentUser_success() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaceHelper.PUBLISHER_AGREEMENT_SCHEMA_PATH)
                        .setHeaderParams(docCreateCreds)
                        .build())
                .doPost(generateSigningSample("nodoc"))
                .run();
    }

    @Test
    void testPost_currentUser_conflict() {
        EndpointTestBuilder.from(POST_CURRENT_CONFLICT).doPost(generateSigningSample("fakeuser")).run();
    }

    @Test
    void testPost_currentUser_conflict_validateResponseFormat() {
        EndpointTestBuilder.from(POST_CURRENT_CONFLICT).doPost(generateSigningSample("fakeuser")).andCheckFormat().run();
    }

    @Test
    void testPost_currentUser_conflict_validateSchema() {
        EndpointTestBuilder.from(POST_CURRENT_CONFLICT).doPost(generateSigningSample("fakeuser")).andCheckFormat().run();
    }

    @Test
    void testPost_currentUser_failure_invalidHandle() {
        EndpointTestBuilder.from(POST_CURRENT_INVALID_HANDLE).doPost(generateSigningSample("otheruser")).run();
    }

    @Test
    void testPost_currentUser_failure_invalidHandle_validateFormat() {
        EndpointTestBuilder.from(POST_CURRENT_INVALID_HANDLE).doPost(generateSigningSample("otheruser")).andCheckFormat().run();
    }

    @Test
    void testPost_currentUser_failure_badCreds() {
        EndpointTestBuilder.from(BAD_CREDS).doPost(generateSigningSample("fakeuser")).run();
    }

    /*
     * GET FOR USER
     */
    @Test
    void testGet_getForUser_success() {
        EndpointTestBuilder.from(GET_USER_SUCCESS).run();
    }

    @Test
    void testGet_geFortUser_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_USER_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGet_getForUser_success_validateSchema() {
        EndpointTestBuilder.from(GET_USER_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGet_getForUser_failure_notFound() {
        EndpointTestBuilder.from(GET_USER_NOT_FOUND).run();
    }

    @Test
    void testGet_getForUser_failure_notFound_validateResponseFormat() {
        EndpointTestBuilder.from(GET_USER_NOT_FOUND).andCheckFormat().run();
    }

    @Test
    void testGet_getForUser_failure_notFound_validateSchema() {
        EndpointTestBuilder.from(GET_USER_NOT_FOUND).andCheckSchema().run();
    }

    @Test
    void testGet_getForUser_failure_badCreds() {
        EndpointTestBuilder.from(FOR_USER_BAD_CREDS).run();
    }

    /*
     * DELETE FOR USER
     */
    @Test
    void testDelete_deleteForUser_success() {
        EndpointTestBuilder.from(REVOKE_SUCCESS).doDelete(null).run();
    }

    @Test
    void testDelete_deleteForUser_failure_invalidUser() {
        EndpointTestBuilder.from(REVOKE_INVALID_USER).doDelete(null).run();
    }

    @Test
    void testDelete_deleteForUser_failure_noDoc() {
        EndpointTestBuilder.from(REVOKE_NO_DOC).doDelete(null).run();
    }

    @Test
    void testDelete_deleteForUser_failure_noDoc_validateSchema() {
        EndpointTestBuilder.from(REVOKE_NO_DOC).doDelete(null).andCheckSchema().run();
    }

    @Test
    void testDelete_deleteForUser_failure_badCreds() {
        EndpointTestBuilder.from(FOR_USER_BAD_CREDS).run();
    }

    private String generateSigningSample(String ghHandle) {
        try {
            return mapper.writeValueAsString(new AgreementSigningRequest("1", ghHandle));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
