/*********************************************************************
* Copyright (c) 2023 Eclipse Foundation.
*
* This program and the accompanying materials are made
* available under the terms of the Eclipse Public License 2.0
* which is available at https://www.eclipse.org/legal/epl-2.0/
*
* Author: Zachary Sabourin <zachary.sabourin@eclipse-foundation.org>
*
* SPDX-License-Identifier: EPL-2.0
**********************************************************************/
package org.eclipsefoundation.openvsx.resources;

import java.util.Map;
import java.util.Optional;

import org.eclipsefoundation.openvsx.test.helpers.SchemaNamespaceHelper;
import org.eclipsefoundation.testing.helpers.TestCaseHelper;
import org.eclipsefoundation.testing.models.EndpointTestBuilder;
import org.eclipsefoundation.testing.models.EndpointTestCase;
import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class ProfileResourceTest {
    public static final String BASE_URL = "profile";

    public static final Optional<Map<String, Object>> userCreds = Optional.of(Map.of("Authorization", "Bearer token2"));

    public static final Optional<Map<String, Object>> noUserCreds = Optional.of(Map.of("Authorization", "Bearer token6"));

    public static final Optional<Map<String, Object>> invalidCreds = Optional.of(Map.of("Authorization", "Bearer token1"));

    public static final EndpointTestCase GET_CURRENT_SUCCESS = TestCaseHelper
            .prepareTestCase(BASE_URL, new String[] {}, SchemaNamespaceHelper.EF_USERS_SCHEMA_PATH)
            .setHeaderParams(userCreds)
            .build();

    /*
     * GET CURRENT USER
     */
    @Test
    void testGetProfile_success() {
        EndpointTestBuilder.from(GET_CURRENT_SUCCESS).run();
    }

    @Test
    void testGetProfile_success_validateResponseFormat() {
        EndpointTestBuilder.from(GET_CURRENT_SUCCESS).andCheckFormat().run();
    }

    @Test
    void testGetProfile_success_validateSchema() {
        EndpointTestBuilder.from(GET_CURRENT_SUCCESS).andCheckSchema().run();
    }

    @Test
    void testGetProfile_failure_anonymousToken() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(BASE_URL, new String[] {}, null)
                        .setStatusCode(403)
                        .setHeaderParams(noUserCreds)
                        .build())
                .run();
    }

    @Test
    void testGetProfile_failure_invalidCreds() {
        EndpointTestBuilder
                .from(TestCaseHelper
                        .prepareTestCase(BASE_URL, new String[] {}, null)
                        .setStatusCode(403)
                        .setHeaderParams(invalidCreds)
                        .build())
                .run();
    }
}
